/*
                          Copyright (C) 2015 A Connor Waters

              Distributed under the Boost Software License, Version 1.0.
    (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef POSSIT_HPP
#   define POSSIT_HPP

#ifndef POSSIT_FAST
#   define POSSIT_LEAN
#endif

#include <cstdint>

#define POSSIT_BIT(n) ( (1) << (n) )

#ifdef POSSIT_FAST
#   define POSSIT_FLAG(id) bool id
#else /* POSSIT_LEAN */
#   define POSSIT_FLAG(id) uint16_t id : 1
#endif

namespace possit {

class x86 final
{
private:
    struct {
        POSSIT_FLAG (mmx);
        POSSIT_FLAG (mmxext);
        POSSIT_FLAG (now);
        POSSIT_FLAG (nowext);

        POSSIT_FLAG (sse);
        POSSIT_FLAG (sse2);
        POSSIT_FLAG (sse3);
        POSSIT_FLAG (ssse3);
        POSSIT_FLAG (sse4a);
        POSSIT_FLAG (sse4_1);
        POSSIT_FLAG (sse4_2);
        POSSIT_FLAG (sse5); // XOP + FMA4 + F16C

        POSSIT_FLAG (avx);
        POSSIT_FLAG (avx2);
        POSSIT_FLAG (avx3_1); // AVX-512F + AVX-512CD
        POSSIT_FLAG (avx3_2); // AVX-512F + AVX-512CD + AVX-512BW + AVX-512DQ + AVX-512VL

        struct {
            POSSIT_FLAG (f);    // Foundation Instruction Set
            POSSIT_FLAG (cd);   // Conflict Detection Instructions
            POSSIT_FLAG (er);   // Exponential and Reciprocal Instructions
            POSSIT_FLAG (pf);   // Prefetch Instructions
            POSSIT_FLAG (bw);   // Byte and Word Instructions
            POSSIT_FLAG (dq);   // Doubleword and Quadword Instructions
            POSSIT_FLAG (vl);   // Vector Length Extensions
            POSSIT_FLAG (ifma); // Integer Fused Multiply-Add Instructions
            POSSIT_FLAG (vbmi); // Vector Byte Manipulation Instructions
        } avx512;
    
        POSSIT_FLAG (fma4);
        POSSIT_FLAG (fma3);
    
        POSSIT_FLAG (f16c);
    
        POSSIT_FLAG (xop);
    } internal_simd {0};
    
    struct {
        POSSIT_FLAG (aes);
        POSSIT_FLAG (sha);
        POSSIT_FLAG (clmul);
        POSSIT_FLAG (rdrand);
        POSSIT_FLAG (rdseed);
    } internal_crypto {0};
    
    struct {
        POSSIT_FLAG (bmi1);
        POSSIT_FLAG (bmi2);
        POSSIT_FLAG (tbm);
        POSSIT_FLAG (lzcnt);
        POSSIT_FLAG (popcnt);
    } internal_bitmanip {0};

    struct {
	POSSIT_FLAG (clflush);
	POSSIT_FLAG (prefetch);
    } internal_cache {0};

    struct {
	POSSIT_FLAG (vmx);
	POSSIT_FLAG (vme);
	POSSIT_FLAG (svm);
    } internal_virt {0};

    struct {
	// TODO: Implement this!
    } internal_state; 

    struct {
	POSSIT_FLAG (smx);
	POSSIT_FLAG (sgx);
	POSSIT_FLAG (smep);
	POSSIT_FLAG (smap);
	POSSIT_FLAG (mpx);
	POSSIT_FLAG (nx);
    } internal_security {0};

    class {
	union {
	    char vstr[13] = {0};
	    struct { uint32_t ebx, edx, ecx; };
	};

    public:
	bool operator == (uint8_t v) const noexcept { return identify (ebx, edx, ecx) == v; }
	operator char const* const() const noexcept { return vstr; }
	friend class x86;
    } internal_vendor;

    static uint8_t identify (uint32_t, uint32_t, uint32_t) noexcept;

public:
    x86() noexcept;

    x86 (x86 const&) = delete;
    x86 (x86 &&)     = delete;

    x86& operator = (x86 const&) = delete;
    x86& operator = (x86 &&)     = delete;

    struct cpuinfo { uint32_t eax, ebx, ecx, edx; };

    decltype (internal_simd)     const& simd     = internal_simd;
    decltype (internal_crypto)   const& crypto   = internal_crypto;
    decltype (internal_bitmanip) const& bitmanip = internal_bitmanip;
    decltype (internal_cache)    const& cache    = internal_cache;
    decltype (internal_virt)     const& virt     = internal_virt;
    decltype (internal_state)    const& state    = internal_state;
    decltype (internal_security) const& security = internal_security;

    decltype (internal_vendor) const& vendor = internal_vendor;

    enum : uint8_t { unknown, intel, amd, via };

    static cpuinfo cpuid (uint32_t eax, uint32_t ecx = 0x00000000) noexcept;
};

x86::x86() noexcept
{
    auto base = cpuid(0x00000000);
    auto extn = cpuid(0x80000000);
    uint32_t max_leaf = base.eax;
    uint32_t ext_leaf = extn.eax;
    
    internal_vendor.ebx = base.ebx;
    internal_vendor.edx = base.edx;
    internal_vendor.ecx = base.ecx;

    if (max_leaf >= 1)
    {
	auto leaf = cpuid(1);

	internal_virt.vme          = bool ( leaf.edx & POSSIT_BIT( 1) );
	internal_cache.clflush     = bool ( leaf.edx & POSSIT_BIT(19) );
	internal_simd.mmx          = bool ( leaf.edx & POSSIT_BIT(23) );
	internal_simd.sse          = bool ( leaf.edx & POSSIT_BIT(25) );
	internal_simd.sse2         = bool ( leaf.edx & POSSIT_BIT(26) );
	internal_simd.sse3         = bool ( leaf.ecx & POSSIT_BIT( 0) );
	internal_crypto.clmul      = bool ( leaf.ecx & POSSIT_BIT( 1) );
	internal_virt.vmx          = bool ( leaf.ecx & POSSIT_BIT( 5) );
	internal_security.smx      = bool ( leaf.ecx & POSSIT_BIT( 6) );
	internal_simd.ssse3        = bool ( leaf.ecx & POSSIT_BIT( 9) );
	internal_simd.fma3         = bool ( leaf.ecx & POSSIT_BIT(12) );
	internal_simd.sse4_1       = bool ( leaf.ecx & POSSIT_BIT(19) );
	internal_simd.sse4_2       = bool ( leaf.ecx & POSSIT_BIT(20) );
	internal_bitmanip.popcnt   = bool ( leaf.ecx & POSSIT_BIT(23) );
	internal_crypto.aes        = bool ( leaf.ecx & POSSIT_BIT(25) );
	internal_simd.avx          = bool ( leaf.ecx & POSSIT_BIT(28) );
	internal_simd.f16c         = bool ( leaf.ecx & POSSIT_BIT(29) );
	internal_crypto.rdrand     = bool ( leaf.ecx & POSSIT_BIT(30) );
    }

    if (max_leaf >= 7)
    {
	auto leaf = cpuid(7);

	internal_security.sgx     = bool ( leaf.ebx & POSSIT_BIT( 2) );
	internal_bitmanip.bmi1    = bool ( leaf.ebx & POSSIT_BIT( 3) );
	internal_simd.avx2        = bool ( leaf.ebx & POSSIT_BIT( 5) );
	internal_security.smep    = bool ( leaf.ebx & POSSIT_BIT( 7) );
	internal_bitmanip.bmi2    = bool ( leaf.ebx & POSSIT_BIT( 8) );
	internal_security.mpx     = bool ( leaf.ebx & POSSIT_BIT(14) );
	internal_simd.avx512.f    = bool ( leaf.ebx & POSSIT_BIT(16) );
	internal_simd.avx512.dq   = bool ( leaf.ebx & POSSIT_BIT(17) );
	internal_crypto.rdseed    = bool ( leaf.ebx & POSSIT_BIT(18) );
	internal_security.smap    = bool ( leaf.ebx & POSSIT_BIT(20) );
	internal_simd.avx512.ifma = bool ( leaf.ebx & POSSIT_BIT(21) );
	internal_simd.avx512.pf   = bool ( leaf.ebx & POSSIT_BIT(26) );
	internal_simd.avx512.er   = bool ( leaf.ebx & POSSIT_BIT(27) );
	internal_simd.avx512.cd   = bool ( leaf.ebx & POSSIT_BIT(28) );
	internal_crypto.sha       = bool ( leaf.ebx & POSSIT_BIT(29) );
	internal_simd.avx512.bw   = bool ( leaf.ebx & POSSIT_BIT(30) );
	internal_simd.avx512.vl   = bool ( leaf.ebx & POSSIT_BIT(31) );
	internal_simd.avx512.vbmi = bool ( leaf.ecx & POSSIT_BIT( 1) );
    }

    if (ext_leaf >= 0x80000001)
    {
	auto leaf = cpuid (0x80000001);
	
	internal_security.nx    = bool ( leaf.edx & POSSIT_BIT(20) );
	internal_bitmanip.lzcnt = bool ( leaf.ecx & POSSIT_BIT( 5) );
	internal_cache.prefetch = bool ( leaf.ecx & POSSIT_BIT( 8) );
	
	if (vendor == amd)
	{
	    internal_simd.mmxext  = bool ( leaf.edx & POSSIT_BIT(22) );
	    internal_simd.mmx     = bool ( leaf.edx & POSSIT_BIT(23) );
	    internal_simd.nowext  = bool ( leaf.edx & POSSIT_BIT(30) );
	    internal_simd.now     = bool ( leaf.edx & POSSIT_BIT(31) );
	    internal_virt.svm     = bool ( leaf.ecx & POSSIT_BIT( 2) );
	    internal_simd.sse4a   = bool ( leaf.ecx & POSSIT_BIT( 6) );
	    internal_simd.xop     = bool ( leaf.ecx & POSSIT_BIT(11) );
	    internal_simd.fma4    = bool ( leaf.ecx & POSSIT_BIT(16) );
	    internal_bitmanip.tbm = bool ( leaf.ecx & POSSIT_BIT(21) );
	}
    }

    internal_simd.sse5 = internal_simd.xop  &&
                         internal_simd.fma4 &&
                         internal_simd.f16c  ;
    
    internal_simd.avx3_1 = internal_simd.avx512.f  &&
                           internal_simd.avx512.cd  ;
    
    internal_simd.avx3_2 = internal_simd.avx512.f  &&
                           internal_simd.avx512.cd &&
                           internal_simd.avx512.bw &&
                           internal_simd.avx512.dq &&
                           internal_simd.avx512.vl  ;
}

uint8_t x86::identify (uint32_t ebx, uint32_t edx, uint32_t ecx) noexcept
{
    if ( ebx == 0x756e6547 && // "Genu"
         edx == 0x49656e69 && // "ineI"
         ecx == 0x6c65746e  ) // "ntel"
	
	return x86::intel;

    if ( ebx == 0x68747541 && // "Auth"
	 edx == 0x69746e65 && // "enti"
	 ecx == 0x444d4163  ) // "cAMD"

	return x86::amd;

    if ( ebx == 0x20414956 && // "VIA "
	 edx == 0x20414956 && // "VIA "
	 ecx == 0x20414956  ) // "VIA "

	return x86::via;

    return x86::unknown;
}

x86::cpuinfo x86::cpuid (uint32_t eax, uint32_t ecx) noexcept
{
    x86::cpuinfo ret;

    asm volatile
    (
        "movl %4, %%eax \n"
        "movl %5, %%ecx \n"
        "cpuid          \n"
        "movl %%eax, %0 \n"
        "movl %%ebx, %1 \n"
        "movl %%ecx, %2 \n"
        "movl %%edx, %3 \n"
    :
        "=rm" (ret.eax),
        "=rm" (ret.ebx),
        "=rm" (ret.ecx),
        "=rm" (ret.edx)
    :
        "rm" (eax),
        "rm" (ecx)
    :
        "eax",
        "ebx",
        "ecx",
        "edx"
    );

    return ret;
}

} // namespace possit

#undef POSSIT_BIT
#undef POSSIT_FLAG

#endif // POSSIT_HPP
