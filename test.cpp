#include <iostream>
#include "possit.hpp"

using possit::x86;
using std::cout;

x86 cpu;

int main()
{
    cout << "Vendor string: " << cpu.vendor << '\n';
    cout << "Sanity check: " << (cpu.vendor == x86::intel ? "Passed!" : "Failed!") << '\n';

    cout << "MMX:          " << cpu.simd.mmx         << '\n';
    cout << "MMX-ext:      " << cpu.simd.mmxext      << '\n';
    cout << "3DNow!:       " << cpu.simd.now         << '\n';
    cout << "3DNow!-ext:   " << cpu.simd.nowext      << '\n';
    cout << "SSE:          " << cpu.simd.sse         << '\n';
    cout << "SSE2:         " << cpu.simd.sse2        << '\n';
    cout << "SSE3:         " << cpu.simd.sse3        << '\n';
    cout << "SSSE3:        " << cpu.simd.ssse3       << '\n';
    cout << "SSE4a:        " << cpu.simd.sse4a       << '\n';
    cout << "SSE4.1:       " << cpu.simd.sse4_1      << '\n';
    cout << "SSE4.2:       " << cpu.simd.sse4_2      << '\n';
    cout << "SSE5:         " << cpu.simd.sse5        << '\n';
    cout << "AVX:          " << cpu.simd.avx         << '\n';
    cout << "AVX2:         " << cpu.simd.avx2        << '\n';
    cout << "AVX3.1:       " << cpu.simd.avx3_1      << '\n';
    cout << "AVX3.2:       " << cpu.simd.avx3_2      << '\n';
    cout << "AVX-512 F:    " << cpu.simd.avx512.f    << '\n';
    cout << "AVX-512 CD:   " << cpu.simd.avx512.cd   << '\n';
    cout << "AVX-512 ER:   " << cpu.simd.avx512.er   << '\n';
    cout << "AVX-512 PF:   " << cpu.simd.avx512.pf   << '\n';
    cout << "AVX-512 BW:   " << cpu.simd.avx512.bw   << '\n';
    cout << "AVX-512 DQ:   " << cpu.simd.avx512.dq   << '\n';
    cout << "AVX-512 VL:   " << cpu.simd.avx512.vl   << '\n';
    cout << "AVX-512 IFMA: " << cpu.simd.avx512.ifma << '\n';
    cout << "AVX-512 VBMI: " << cpu.simd.avx512.vbmi << '\n';
    cout << "FMA4:         " << cpu.simd.fma4        << '\n';
    cout << "FMA3:         " << cpu.simd.fma3        << '\n';
    cout << "F16C:         " << cpu.simd.f16c        << '\n';
    cout << "XOP:          " << cpu.simd.xop         << '\n';

    cout << "AES:          " << cpu.crypto.aes       << '\n';
    cout << "SHA1:         " << cpu.crypto.sha       << '\n';
    cout << "PCLMULQDQ:    " << cpu.crypto.clmul     << '\n';
    cout << "RDRND:        " << cpu.crypto.rdrand    << '\n';
    cout << "RDSEED:       " << cpu.crypto.rdseed    << '\n';

    cout << "BMI1:         " << cpu.bitmanip.bmi1    << '\n';
    cout << "BMI2:         " << cpu.bitmanip.bmi2    << '\n';
    cout << "TBM:          " << cpu.bitmanip.tbm     << '\n';
    cout << "LZCNT:        " << cpu.bitmanip.lzcnt   << '\n';
    cout << "POPCNT:       " << cpu.bitmanip.popcnt  << '\n';

    cout << "CLFLUSH:      " << cpu.cache.clflush    << '\n';
    cout << "PREFETCH:     " << cpu.cache.prefetch   << '\n';

    cout << "VMX:          " << cpu.virt.vmx         << '\n';
    cout << "VME:          " << cpu.virt.vme         << '\n';
    cout << "SVM:          " << cpu.virt.svm         << '\n';

    cout << "SMX:          " << cpu.security.smx     << '\n';
    cout << "SGX:          " << cpu.security.sgx     << '\n';
    cout << "SMEP:         " << cpu.security.smep    << '\n';
    cout << "SMAP:         " << cpu.security.smap    << '\n';
    cout << "MPX:          " << cpu.security.mpx     << '\n';
    cout << "NX:           " << cpu.security.nx      << '\n';

    return 0;
}
